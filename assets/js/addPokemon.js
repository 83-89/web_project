$(function () {
    $.ajaxSetup({
        async: false
    });

    var dialog = document.getElementById('addPokemonDialog');
    var presenceDialog = document.getElementById('pokemonPresenceDialog');

    $("#addPokemon").on("click", function () {
        dialog.showModal();
    });

    $("#submit").on("click", function (e) {
        var pokeId = $("#default_select").val();
        var isPresent = false;
        $.getJSON(`/api/trainers/${trainerId}`, function (data) {
            if (data[0].pokemons.length >= 1) {
                for (var i = 0; i < data[0].pokemons.length; i++) {
                    if (pokeId == data[0].pokemons[i].pokeId) {
                        isPresent = true;
                    }
                }
            }
        });

        if (isPresent) {
            e.preventDefault();
            presenceDialog.showModal();
        }
    });

    var cancelButton = document.getElementById('cancelButton');
    var OKButton = document.getElementById('OKButton');

    cancelButton.addEventListener('click', function (e) {
        e.preventDefault();
        dialog.close('pokemonNotChosen');
    });

    OKButton.addEventListener('click', function (e) {
        presenceDialog.close('pokemonPresent');
    });
});
