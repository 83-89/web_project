$(function () {
    $.ajaxSetup({
        async: false
    });

    var dialog = document.getElementById('newTrainerDialog');

    $("#signInButton").on("click", function (e) {
        var currentForm = $(this).closest('form');
        var name = $("#name_field").val();
        var gender = $("input[name='gender']:checked").val();

        var trainerPresence = false;
        $.getJSON("/api/trainers/", function (data) {
            if (data.length >= 1) {
                for (var i = 0; i < data.length; i++) {
                    if ((name == data[i].name) && (gender == data[i].genre)) {
                        trainerPresence = true;
                        break;
                    }
                }
            }
        });

        if (!trainerPresence) {
            e.preventDefault();
            dialog.showModal();
        }

        var cancelButton = document.getElementById('cancelButton');
        cancelButton.addEventListener('click', function (e) {
            e.preventDefault();
            dialog.close('trainerNotCreated');
        });

        var validateButton = document.getElementById('validateButton');
        validateButton.addEventListener('click', function () {
            dialog.close('trainerCreated');
            currentForm.submit();
        })
    });


});
