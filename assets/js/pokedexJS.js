const capitalize = (s) => {
    if (typeof s !== 'string') return '';
    const cap=s.charAt(0).toUpperCase();
    const slice=s.slice(1);
    return `${cap}${slice}`;
}

var num = 0;

function reset() {
    $('.pokenum').empty();
    $('.sprite').empty();
    $('.name').empty();
    $('.height').empty();
    $('.weight').empty();
    $('.types').empty();
}

function getPokemon() {
    $.getJSON('/api/pokemons', function (data) {
        const pokeId = data[num].pokeId;
        const imageUrl = data[num].imageUrl;
        const name=data[num].name;
        if (pokeId < 100) {
            if (pokeId < 10) {
                $('.pokenum').append(`#00${pokeId}`);
                $('.sprite').append(`<img src=${imageUrl} alt=${name}/>`);
            }
            else {
                $('.pokenum').append(`#0${pokeId}`);
                $('.sprite').append(`<img src=${imageUrl} alt=${name}/>`);
            }
        }
        else {
            $('.pokenum').append(`#${pokeId}`);
            $('.sprite').append(`<img src=${imageUrl} alt=${name}/>`);
        }
        for (var i = 0; i < data[num].types.length; i++) {
            const typeName = data[num].types[i].name;
            $('.types').append(`<img src=../assets/img/${typeName}.png alt=${typeName}/>`);
        }
    });
}

function getDescription() {
    $.getJSON('/api/pokemons', function (data) {
        const name=data[num].name;
        const height = data[num].height / 10;
        const weight = data[num].weight / 10;
        $('.name').append(capitalize(`${name}`));
        $('.height').append(`${height}m`);
        $('.weight').append(`${weight}kg`);
    });
}

getPokemon();
getDescription();

$('.find').on("click", function () {
    reset();
    num = $(this).parent().attr('value') - 1;
    getPokemon();
    getDescription();
});