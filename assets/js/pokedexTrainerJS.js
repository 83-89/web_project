const capitalize = (s) => {
    if (typeof s !== 'string') return '';
    const cap = s.charAt(0).toUpperCase();
    const slice = s.slice(1);
    return `${cap}${slice}`;
}

var num = 0;
var url = window.location.pathname;
var trainerId = url.substring(url.lastIndexOf('/') + 1);

function reset() {
    $('.pokenum').empty();
    $('.sprite').empty();
    $('.name').empty();
    $('.height').empty();
    $('.weight').empty();
    $('.types').empty();
}

function getPokemon(trainerId) {
    $.getJSON(`/api/trainers/${trainerId}`, function (data) {
        if (data[0].pokemons.length >= 1) {
            const pokeId = data[0].pokemons[num].pokeId;
            const imageUrl = data[0].pokemons[num].imageUrl;
            const name = data[0].pokemons[num].name;
            if (pokeId < 100) {
                if (pokeId < 10) {
                    $('.pokenum').append(`#00${pokeId}`);
                    $('.sprite').append(`<img src=${imageUrl} alt=${name} />`);
                }
                else {
                    $('.pokenum').append(`#0${pokeId}`);
                    $('.sprite').append(`<img src=${imageUrl} alt=${name} />`);
                }
            }
            else {
                $('.pokenum').append(`#${pokeId}`);
                $('.sprite').append(`<img src=${imageUrl} alt=${name} />`);
            }
            for (var i = 0; i < data[0].pokemons[num].types.length; i++) {
                const typeName = data[0].pokemons[num].types[i].name;
                $('.types').append(`<img src=../assets/img/${typeName}.png alt=${typeName} />`);
            }
        }
    });
}

function getDescription(trainerId) {
    $.getJSON(`/api/trainers/${trainerId}`, function (data) {
        if (data[0].pokemons.length >= 1) {
            const name = data[0].pokemons[num].name;
            const height = data[0].pokemons[num].height / 10;
            const weight = data[0].pokemons[num].weight / 10;
            $('.name').append(capitalize(`${name}`));
            $('.height').append(`${height}m`);
            $('.weight').append(`${weight}kg`);
        }
    });
}

getPokemon(trainerId);
getDescription(trainerId);

$('.find').on("click", function () {
    reset();
    num = $(this).parent().attr('value');
    getPokemon(trainerId);
    getDescription(trainerId);
});