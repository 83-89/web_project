<?php
class Pokemon extends Model {
    public static $_table = 'pokemon';
    public static $_id_column = 'pokeId';
    public function type(){
        return $this->has_many_through('Type', 'isOfType', 'pokeId', 'typeId');
    }
    public function trainer(){
        return $this->has_many_through('Trainer', 'Possess', 'pokeId', 'trainerId');
    }
}

class Type extends Model {
    public static $_table = 'type';
    public static $_id_column = 'typeId';
    public function pokemon(){
        return $this->has_many_through('Pokemon', 'isOfType', 'typeId', 'pokeId');
    }
}

class Trainer extends Model {
    public static $_table = 'trainer';
    public static $_id_column = 'trainerId';
    public function pokemon(){
        return $this->has_many_through('Pokemon', 'Possess', 'trainerId', 'pokeId');
    }
}

class isOfType extends Model {
    public static $_table='isOfType';
}

class Possess extends Model {
    public static $_table='possess';
}
?>