<?php
use function GuzzleHttp\json_decode;

//function getTypes
//The function collects the type data from the API pokeapi.co
//IN : nothing
//OUT : array containing the Pokemon types
function getTypes()
{
    $response = Requests::get("https://pokeapi.co/api/v2/type");
    return json_decode($response->body);
}

//function getPokemonInfo
//The function collects all the data for a selected Pokemon from the API pokeapi.co
//IN : id of a Pokemon
//OUT : array containing the Pokemon infos
function getPokemonInfo($id)
{
    $path = sprintf("https://pokeapi.co/api/v2/pokemon/%s", $id);
    $response = Requests::get($path);
    return json_decode($response->body);
}

//function getPokemonTypes
//The function collects the types for a selected Pokemon from the API pokeapi.co
//IN : id of a Pokemon
//OUT : array containing the types of the Pokemon
function getPokemonTypes($id)
{
    $pokemonTypes = array();

    foreach (getPokemonInfo($id)->types as $type) {
        $pokemonTypes[] = $type->type->name;
    }
    return $pokemonTypes;
}

//function getPokemons
//The function collects the needful data for all the 1st gen Pokemons
//IN : nothing
//OUT : array containing the data of the Pokemon
function getPokemons()
{
    $pokemon = array();

    for ($i = 1; $i <= 151; $i++) {
        $pokemon[] = array(
            "name" => getPokemonInfo($i)->name,
            "height" => getPokemonInfo($i)->height,
            "weight" => getPokemonInfo($i)->weight,
            "imageUrl" => getPokemonInfo($i)->sprites->front_default,
        );
    }

    return $pokemon;
}

//function addPokemonsToDB
//The function fills up the table Pokemon (database pokedex.sqlite3)
//IN : nothing
//OUT : nothing
function addPokemonsToDB()
{
    $pokemon = getPokemons();

    foreach ($pokemon as $poke) {
        $newPoke = Model::factory('Pokemon')->create();
        $newPoke->name = $poke["name"];
        $newPoke->height = $poke["height"];
        $newPoke->weight = $poke["weight"];
        $newPoke->imageUrl = $poke["imageUrl"];
        $newPoke->save();
    }
}

//function addTypesToDB
//The function fills up the table Type (database pokedex.sqlite3)
//IN : nothing
//OUT : nothing
function addTypesToDB()
{
    $types = getTypes()->results;

    foreach ($types as $type) {
        $newType = Model::factory('Type')->create();
        $newType->name = $type->name;
        $newType->save();
    }
}

//function addPokemonTypesToDB
//The function fills up the table isOfType (database pokedex.sqlite3)
//IN : nothing
//OUT : nothing
function addPokemonTypesToDB()
{
    for ($i = 1; $i <= 151; $i++) {
        foreach (getPokemonTypes($i) as $type) {
            $pokemonType = Type::where('name', $type)->find_one();
            $new = Model::factory('isOfType')->create();
            $new->pokeId = $i;
            $new->typeId = $pokemonType->typeId;
            $new->save();
        }
    }
}

//function getPokemonsForAPI
//The function collects the data from the Pokemon table
//IN : nothing
//OUT : array containing the data from the Pokemon table
function getPokemonsForAPI()
{
    $pokemons = Pokemon::factory('Pokemon')->find_many();
    return $pokemons;
}

//function getPokemonTypeForAPI
//The function collects the types of a selected Pokemon from the isOfType table
//IN : id of a Pokemon
//OUT : array containing the types of the selected Pokemon
function getPokemonTypeForAPI($id)
{
    $result = array();
    $pokemon = Pokemon::factory('Pokemon')->find_one($id);
    foreach ($pokemon->type()->find_many() as $type) {
        $result[] = array(
            'typeId' => $type->typeId,
            'name' => $type->name,
        );
    }
    return $result;
}

//function getPokemonsArrayForAPI
//The function collects the data and type(s) of all the Pokemons for the API
//IN : nothing
//OUT : array containing the data (with type(s)) of all the Pokemons
function getPokemonsArrayForAPI()
{
    $result = array();
    foreach (getPokemonsForAPI() as $poke) {
        $result[] = array(
            'pokeId' => $poke->pokeId,
            'name' => $poke->name,
            'height' => $poke->height,
            'weight' => $poke->weight,
            'imageUrl' => $poke->imageUrl,
            'types' => getPokemonTypeForAPI($poke->pokeId),
        );
    }
    return $result;
}

//function getTrainersForAPI
//The function collects all trainers from the Trainer table
//IN : nothing
//OUT : array containing the trainers
function getTrainersForAPI()
{
    $result = array();
    $trainers = Trainer::factory('Trainer')->find_many();
    foreach ($trainers as $trainer) {
        $result[] = array(
            'trainerId' => $trainer->trainerId,
            'name' => $trainer->name,
            'genre' => $trainer->genre,
            'uri' => sprintf("/api/trainer/%d", $trainer->trainerId),
        );
    }
    return $result;
}

//function getTrainerPokemonsForAPI
//The function collects the pokemons of a selected Trainer from the Possess table
//IN : id of a Trainer
//OUT : array containing the pokemons of the selected Trainer
function getTrainerPokemonsForAPI($id)
{
    $result = array();
    $trainer = Trainer::factory('Trainer')->find_one($id);
    foreach ($trainer->pokemon()->order_by_asc('pokeId')->find_many() as $poke) {
        $result[] = array(
            'pokeId' => $poke->pokeId,
            'name' => $poke->name,
            'height' => $poke->height,
            'weight' => $poke->weight,
            'imageUrl' => $poke->imageUrl,
            'types' => getPokemonTypeForAPI($poke->pokeId),
        );
    }
    return $result;
}

//function getTrainerArrayForAPI
//The function collects the data and pokemon(s) of all the Trainers for the API
//IN : id of a Trainer
//OUT : array containing the data (with pokemon(s)) of the selected Trainer
function getTrainerArrayForAPI($id)
{
    $trainer = Trainer::factory('Trainer')->find_one($id);
    if ($trainer != NULL) {
        $result = array();
        $result[] = array(
            'trainerId' => $trainer->trainerId,
            'name' => $trainer->name,
            'genre' => $trainer->genre,
            'pokemons' => getTrainerPokemonsForAPI($trainer->trainerId),
        );
        return $result;
    }
    $errorMessage = sprintf("The id #%d doesn't match any Trainer.", $id);
    return $errorMessage;
}
