<?php require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{
    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("And remember, gotta catch 'em all!", $response->getBody()->getContents());
        $response = $this->make_request("GET", "/");
        $this->assertStringContainsString("Pokedex Manager", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_pokedex()
    {
        $response = $this->make_request("GET", "/pokedex");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Bulbasaur", $response->getBody()->getContents());
        $response = $this->make_request("GET", "/pokedex");
        $this->assertStringContainsString("Pikachu", $response->getBody()->getContents());
        $response = $this->make_request("GET", "/pokedex");
        $this->assertStringContainsString("Mew", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_pokedex_trainer()
    {
        $response = $this->make_request("GET", "/pokedex/1");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Bulbasaur", $response->getBody()->getContents());
        $response = $this->make_request("GET", "/pokedex");
        $this->assertStringContainsString("Mew", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);

        $response = $this->make_request("GET", "/pokedex/2");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Persian", $response->getBody()->getContents());
        $response = $this->make_request("GET", "/pokedex");
        $this->assertStringContainsString("Rapidash", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }
    
    public function test_api_pokemons()
    {
        $response = $this->make_request("GET", "/api/pokemons");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("application/json", $response->getHeader('Content-Type')[0]);

        $body = $response->getBody()->getContents();

        $this->assertStringContainsString('bulbasaur', $body);
        $this->assertStringContainsString('pikachu', $body);
        $this->assertStringContainsString('mew', $body);
        $this->assertStringContainsString('poison', $body);
        $this->assertStringContainsString('grass', $body);
        $this->assertStringContainsString('electric', $body);
        $this->assertStringContainsString('psychic', $body);
        $this->assertStringContainsString('pikachu', $body);
        $this->assertStringContainsString('https:\/\/raw.githubusercontent.com\/PokeAPI\/sprites\/master\/sprites\/pokemon\/1.png', $body);
    }

    public function test_api_trainers()
    {
        $response1 = $this->make_request("GET", "/api/trainers/1");
        $this->assertEquals(200, $response1->getStatusCode());
        $this->assertStringContainsString("application/json", $response1->getHeader('Content-Type')[0]);

        $body1 = $response1->getBody()->getContents();

        $this->assertStringContainsString('bulbasaur', $body1);
        $this->assertStringContainsString('mew', $body1);
        $this->assertStringContainsString('poison', $body1);
        $this->assertStringContainsString('grass', $body1);
        $this->assertStringContainsString('psychic', $body1);
        $this->assertStringContainsString('clemence', $body1);
        $this->assertStringContainsString('https:\/\/raw.githubusercontent.com\/PokeAPI\/sprites\/master\/sprites\/pokemon\/1.png', $body1);

        $response2 = $this->make_request("GET", "/api/trainers/2");
        $this->assertEquals(200, $response2->getStatusCode());
        $this->assertStringContainsString("application/json", $response2->getHeader('Content-Type')[0]);

        $body2 = $response2->getBody()->getContents();

        $this->assertStringContainsString('persian', $body2);
        $this->assertStringContainsString('rapidash', $body2);
        $this->assertStringContainsString('normal', $body2);
        $this->assertStringContainsString('fire', $body2);
        $this->assertStringContainsString('sebastien', $body2);
        $this->assertStringContainsString('https:\/\/raw.githubusercontent.com\/PokeAPI\/sprites\/master\/sprites\/pokemon\/53.png', $body2);
    }
}
