<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase
{

    public static function setUpBeforeClass(): void
    {
        ORM::configure('sqlite:test.sqlite3');
    }

    public function test_getTypes()
    {
        $types = getTypes()->results;
        $this->assertIsArray($types);
        $this->assertEquals(20, count($types));
        $this->assertIsString($types[0]->name);
        $this->assertIsString($types[19]->url);
    }

    public function test_getPokemonInfo()
    {
        $this->assertEquals('bulbasaur', getPokemonInfo(1)->name);
        $this->assertEquals(7, getPokemonInfo(1)->height);
        $this->assertEquals(69, getPokemonInfo(1)->weight);
        $this->assertEquals('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png', getPokemonInfo(1)->sprites->front_default);
        $this->assertEquals('mew', getPokemonInfo(151)->name);
        $this->assertEquals(4, getPokemonInfo(151)->height);
        $this->assertEquals(40, getPokemonInfo(151)->weight);
        $this->assertEquals('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/151.png', getPokemonInfo(151)->sprites->front_default);
    }

    public function test_getPokemonTypes()
    {
        $this->assertContains('poison', getPokemonTypes(1));
        $this->assertContains('grass', getPokemonTypes(1));
        $this->assertContains('psychic', getPokemonTypes(151));
        $this->assertContains('electric', getPokemonTypes(25));
    }

    /* too too long test (about 2 minutes)

    public function test_getPokemons()
    {
    $pokemon = getPokemons();
    $this->assertStringContainsString('pikachu', $pokemon[24]["name"]);
    $this->assertStringContainsString('bulbasaur', $pokemon[0]["name"]);
    $this->assertStringContainsString('mew', $pokemon[150]["name"]);
    }
     */

    public function test_getPokemonsForAPI()
    {
        $pokes = getPokemonsForAPI();
        $this->assertIsArray($pokes);
        $this->assertStringContainsString("bulbasaur", $pokes[0]->name);
        $this->assertStringContainsString("pikachu", $pokes[24]->name);
        $this->assertStringContainsString("mew", $pokes[150]->name);
    }

    public function test_getPokemonTypeForAPI()
    {
        $result_bulbasaur = getPokemonTypeForAPI(1);
        $this->assertIsArray($result_bulbasaur);
        $this->assertStringContainsString("poison", $result_bulbasaur[0]["name"]);
        $this->assertStringContainsString("grass", $result_bulbasaur[1]["name"]);

        $result_pikachu = getPokemonTypeForAPI(25);
        $this->assertIsArray($result_pikachu);
        $this->assertStringContainsString("electric", $result_pikachu[0]["name"]);

        $result_mew = getPokemonTypeForAPI(151);
        $this->assertIsArray($result_mew);
        $this->assertStringContainsString("psychic", $result_mew[0]["name"]);
    }

    public function test_getPokemonsArrayForAPI()
    {
        $result = getPokemonsArrayForAPI();
        $this->assertIsArray($result);
        $this->assertEquals(count($result), 151);
        $this->assertStringContainsString("bulbasaur", $result[0]["name"]);
        $this->assertStringContainsString("pikachu", $result[24]["name"]);
        $this->assertStringContainsString("mew", $result[150]["name"]);
    }

    public function test_getTrainersForAPI()
    {
        $result = getTrainersForAPI();
        $this->assertIsArray($result);
        $this->assertStringContainsString("clemence", $result[0]["name"]);
        $this->assertStringContainsString("sebastien", $result[1]["name"]);
        $this->assertEquals(0, $result[0]["genre"]);
        $this->assertEquals(1, $result[1]["genre"]);

    }

    public function test_getTrainerPokemonsForAPI()
    {
        $result_trainer_pokemons_1 = getTrainerPokemonsForAPI(1);
        $this->assertIsArray($result_trainer_pokemons_1);
        $this->assertStringContainsString("bulbasaur", $result_trainer_pokemons_1[0]["name"]);
        $this->assertStringContainsString("mew", $result_trainer_pokemons_1[1]["name"]);

        $result_trainer_pokemons_2 = getTrainerPokemonsForAPI(2);
        $this->assertIsArray($result_trainer_pokemons_2);
        $this->assertStringContainsString("persian", $result_trainer_pokemons_2[0]["name"]);
        $this->assertStringContainsString("rapidash", $result_trainer_pokemons_2[1]["name"]);

    }

    public function test_getTrainerArrayForAPI()
    {
        $result_trainer_1 = getTrainerArrayForAPI(1);
        $this->assertIsArray($result_trainer_1);
        $this->assertStringContainsString("clemence", $result_trainer_1[0]["name"]);
        $this->assertStringContainsString("bulbasaur", $result_trainer_1[0]["pokemons"][0]["name"]);
        $this->assertStringContainsString("mew", $result_trainer_1[0]["pokemons"][1]["name"]);

        $result_trainer_2 = getTrainerArrayForAPI(2);
        $this->assertIsArray($result_trainer_2);
        $this->assertStringContainsString("sebastien", $result_trainer_2[0]["name"]);
        $this->assertStringContainsString("persian", $result_trainer_2[0]["pokemons"][0]["name"]);
        $this->assertStringContainsString("rapidash", $result_trainer_2[0]["pokemons"][1]["name"]);

        $result_trainer_42 = getTrainerArrayForAPI(42);
        $this->assertIsNotArray($result_trainer_42);
        $this->assertStringContainsString("The id #42 doesn't match any Trainer.", $result_trainer_42);
    }
}
