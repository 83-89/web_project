﻿## WebProject - Pokedex ![](assets/img/pokeballIcon.png)

## About

A "simple" repo for a web development class' assesment.

- [Composer](https://getcomposer.org/)
- [FlightPHP](http://flightphp.com/) 
- [Twig](https://twig.symfony.com/)
- [Requests](http://requests.ryanmccue.info/)
- [PHPUnit](https://phpunit.de/)
- [Paris](http://j4mie.github.io/idiormandparis/)
- and lots of other stuff

## How to ?
`composer install`

### Run Tests
`vendor/bin/phpunit tests/`

### Run the Server
1. `php -S 127.0.0.1:8000`
2. Then go to [127.0.0.1:8000](http://127.0.0.1:8000)


## Requirements

```
mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
j4mie/paris
```

## Authors

> Clémence CHOMEL & Sébastien CORSYN
