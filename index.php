<?php require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('_post', $_POST);
});

Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:pokedex.sqlite3');
});

Flight::map('notFound', function () {
    // Display custom 404 page
    Flight::view()->display('errors/404.twig');
});

Flight::route('/api/pokemons', function () {
    Flight::json(getPokemonsArrayForAPI());
});

Flight::route('/api/trainers', function () {
    Flight::json(getTrainersForAPI());
});

Flight::route('/api/trainers/@trainerId', function ($trainerId) {
    Flight::json(getTrainerArrayForAPI($trainerId));
});

Flight::route('/', function () {
    if (Flight::request()->method == 'POST') {
        $name = strtolower(Flight::request()->data->name_field);
        $genre = Flight::request()->data->gender;

        $checkTrainerPresence = Trainer::where('name', $name)->where('genre', $genre)->find_one();

        if ($checkTrainerPresence == NULL) {
            $signin = Model::factory('Trainer')->create();
            $signin->name = $name;
            $signin->genre = $genre;
            $signin->save();
        }

        $trainer = Trainer::where('name', $name)->where('genre', $genre)->find_one();
        $path = sprintf("/pokedex/%d", $trainer->trainerId);
        Flight::redirect($path);
    } else {
        Flight::view()->display('signIn.twig');
    }
});

Flight::route('/pokedex', function () {
    $pokemons = Model::factory('Pokemon')->find_many();
    $data = [
        "pokemons" => $pokemons,
    ];
    Flight::view()->display('pokedex.twig', $data);
});

Flight::route('/pokedex/@trainerId', function ($trainerId) {
    if (Flight::request()->method == 'POST') {
        $pokeId = Flight::request()->data->pokeId;
        $trainerId = Flight::request()->data->trainerId;

        $checkPokePresence = Possess::where('pokeId', $pokeId)->where('trainerId', $trainerId)->find_one();

        if ($checkPokePresence == NULL) {
            $possession = Model::factory('Possess')->create();
            $possession->pokeId = $pokeId;
            $possession->trainerId = $trainerId;
            $possession->save();
        }
    }

    $pokemons = Model::factory('Pokemon')->find_many();
    $trainer = Trainer::factory('Trainer')->find_one($trainerId);
    if ($trainer != NULL) {
        $data = [
            "trainerInfo" => $trainer,
            "trainerPokemons" => $trainer->pokemon()->order_by_asc('pokeId')->find_many(),
            "pokemons" => $pokemons,
        ];
        Flight::view()->display('pokedexTrainer.twig', $data);
    } else {
        Flight::notFound();
    }
});

Flight::start();
